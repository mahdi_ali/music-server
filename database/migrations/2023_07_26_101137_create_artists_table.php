<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('artists', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable(false);
            $table->string('artist_img_url')->nullable(false);
            $table->longText('biography');
            $table->string('twitter_address');
            $table->string('instagram_address');
            $table->string('youtube_address');
            $table->string('email_address');
            $table->string('website_address');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('artists');
    }
};
